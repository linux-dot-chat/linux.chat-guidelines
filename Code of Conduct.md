# Linux.Chat Community Code of Conduct

If you have any questions or need further clarification regarding any part of this Code of Conduct, please do not hesitate to contact our staff. You can reach out to us using the [feedback form](https://linux.chat/give-feedback/) provided on our website.


## Community Vision

The purpose of our community is to provide an open, positive, fun, friendly, welcoming and safe environment for all members. We welcome everyone, provided they abide by our community code of conduct and contribute positively.


## Our Pledge to Each Other

In the interest of fostering an open, positive, fun, friendly, welcoming and safe environment, we as Linux.Chat community members pledge to make our participation a harassment-free, pleasant and enjoyable experience for everyone.


## Diversity is Encouraged

Linux.Chat aims to provide a positive, fun, friendly and safe environment not only to everyone in its communities but also to those who interact with us, regardless of:
- age;
- body size;
- caste;
- disability;
- ethnicity;
- gender identity and expression;
- level of experience;
- education;
- socio-economic status;
- nationality;
- personal appearance;
- race;
- religion (or lack thereof);
- sexual identity and orientation;
- tribe; or
- other protected status as defined by laws or the [Universal Declaration of Human Rights](https://www.un.org/en/about-us/universal-declaration-of-human-rights)


## Our Standards

### Positive Behavior

Examples of behavior that contributes to creating an open, positive, fun, friendly, welcoming and safe environment include, but are not limited to:
- Using welcoming, inclusive and friendly language;
- Being respectful of differing viewpoints and experiences;
- Demonstrating empathy and kindness toward other people;
- Giving and gracefully accepting constructive feedback;
- Assume good intent in all interactions, recognizing that differing cultures and perspectives may influence communication styles;
- Accepting responsibility and apologizing to those affected by our mistakes, and learning from the experience; and
- Focusing on what is best not just for us as individuals, but for the overall community.

### Unacceptable Behavior

Examples of unacceptable behavior by Linux.Chat community members include, but are not limited to:
- Trolling. The act of intentionally provoking, disrupting, or derailing a conversation by posting meaningless, illogical, inflammatory, irrelevant, or offensive comments with the aim of eliciting a reaction rather than contributing constructively to the discussion.
- Statements, jokes, or any other content that is insulting, derogatory, or targets demographic groups, including the use of racial or ableist slurs. The use of such slurs will be treated consistently regardless of the background of the user employing them. This ensures consistent enforcement of our code of conduct.
- Deliberate intimidation, stalking, or following.
- Public or private harassment.
- Publishing others’ private information without explicit permission, this includes but is not limited to physical or electronic addresss, phone numbers, social security or identity numbers, tax identification numbers, online identifying information (eg. nicknames, handles or IP addresses).
- Sexual language, media, attention, or advances towards others, or behavior that contributes to a sexualized environment.
- Violence directed against another person or animal, including threats, language or media.
- Uploading or creating content (including images, audio, video) that violates any aspect of our Code of Conduct.
- Discussions related to politics or religion.
- Displaying elitist behavior, such as belitting others for their choices or assuming superiority.
- Using a username, nickname, profile picture, about me, profile page or status message that contains content violating any part of our Code of Conduct.
- Expressing support for such unacceptable behaviors by others.

The following are specific topics that have come up frequently enough to make specific mention as being unacceptable:
- Hacking in the sense of gaining unauthorized access to any system, device, software or intellectual property. This includes but is not limited to password cracking, game hacks/cheats and changing HWIDs.
- Emulators: Discussing the use of emulators is acceptable as long as the discussion does not relate to software being obtained illegally.
- Torrents/Metalinks: Discussing torrents, metalinks and the like for purposes which do not clearly relate to GNU/Linux®.
- Intentionally sharing destructive commands, including as jokes, media, or memes.
- Linking to, or referencing content which violates our Code of Conduct.

Examples of unacceptable behavior by Linux.Chat staff members include, but are not limited to:
- Discussing staff matters publically.
- Exploiting staff privileges to assert dominance, win arguments, or prove a point.


## Rules

Our rules are governed by our Code of Conduct and in the spirit of our Community Vision.

In instances where a specific rule is not available to address a particular situation, members are expected to use common sense and refer to the principles outlined in our Code of Conduct to determine if their actions would be in line with our Positive Behavior guidelines. By doing so, we ensure that our actions remain consistent with our shared values and principles, fostering a respectful and cohesive environment for everyone.


### General Rules


#### 1. English Only

Communicating in English is required in our community to ensure clear and effective communication among all members.

As a diverse and global community, English serves as a common language that most members understand, enabling seamless interaction, collaboration, and sharing of ideas.

This requirement helps to avoid misunderstandings and ensures that everyone can participate equally in discussions, access important information, and contribute to the community's growth.

This rule is not enforced for our Linux.Social platform.


#### 2. No Breaking Terms of Service

Engaging in discussions or joking about breaking any Terms of Service is prohibited. This applies to our own community platforms as well as any platform we operate on.

Respecting Terms of Service is critical for several reasons:
- **Legal Compliance:** Adhering to Terms of Service helps ensure that we operate within legal boundaries and avoid potential legal issues or backlash.
- **Community Integrity:** Following Terms of Service maintains the trust and integrity of our community, promoting a respectful and safe environment for all members.
- **Platform Relations:** Violating Terms of Service of platforms we use can result in penalties, restrictions, or bans, which can negatively impact our community’s ability to operate effectively.

It is essential to refrain from any discussions, whether serious or in jest, that suggest or imply breaking Terms of Service. This commitment helps protect both our community and our presence on various platforms.


#### 3. Minimum Age Requirement

To comply with legal regulations, all members of our community must be at least 13 years old.

This age requirement ensures that we adhere to applicable laws and provide a safe and appropriate environment for all participants.

Any violation of this rule (including joking about being younger than this) will result in immediate removal from the community.


#### 4. No NSFW Content

NSFW is defined as content that is *Not Safe For Work*. You may not send NSFW content in any shape or form, including images, links, profile pictures, statuses, stickers or emotes.

Examples of NSFW content include, but are not limited to:
- Nudity, pornography, sexually suggestive material or comments, gore, violence, homophobia and racism.
- Minors portrayed in inappropriate situations.
- Disturbing material or material intended to deceive, including material that appears NSFW but is not.
- Anything sensitive to the eyes or ears, including colorful flashing images or loud audio.
- Excessive swearing.
- Asking for or "trading" images, such as hand pictures for feet pictures.
- Links or references to any of the above.


#### 5. No Spamming or Flooding

This includes text, images, links, emojis, stickers and reactions.

Spamming is defined as sending messages rapidly or on multiple lines or editing messages rapidly.

Flooding is sending overly long messages, such as walls of text which causes a disruption in the flow of communication.


#### 6. No Advertising

Advertising includes sending content that promotes something, including but not limited to other communities, social media pages, projects ... etc.

Advertising also includes indirectly promoting something by talking about it in an attempt to lure or interest members. For example mentioning something about another community, or asking someone to join a community or check your social media page.

Advertising of Linux-related products, services, opportunities may be permitted within certain areas of the community and permission may be granted by staff in certain circumstances.


#### 7. No Harassment

Harassment includes abusive, insulting, provoking, threatening, derogatory, condescending and or demeaning language use towards other persons. This also includes any form of sexism, racism or homophobia.

Excessively mentioning other persons or creating alt accounts to bypass moderation is considered harassment.

Any form of consistent, repetitive or bothersome behavior targeted towards other persons is considered harassment, including trolling or anything clearly intended to be intimidating.

Doxing (disclosure of sensitive or personally identifiable information) is also considered harassment and is against the law in many countries.


#### 8. No Defamation

No defamation of character is allowed. False statements that damages the reputation of another person will not be tolerated.


#### 9. No Impersonation

Impersonation includes, but is not limited to the following:
- Copying another persons or bots profile picture, name, nickname or username.
- Falsely claiming to be a staff member.
- Falsely claiming that you are the person in an image (catfishing).


#### 10. No Toxicity

Toxicity includes, but is not limited to:
- Starting arguments.
- Name calling.
- Excessive language use.
- Backhanded or malicious comments.
- Purposely making other members uncomfortable.
- Trying to start drama, including bringing drama from other communities to our community.

Keep your interactions with others pleasant and civil.


#### 11. No Raid Discussions

Raiding is defined as a coordinated effort that disrupts any platform or persons experience, including but not limited to online chat platforms, meeting platforms or games.

Discussion of, or joking about raiding will not be tolerated.

Discussions relating to mass reporting of user accounts is also considered raiding.


#### 12. No Sub-Communities

Sub communities are defined as communities that form with the sole purpose of, or engages in, soliciting members from our community.

Examples of what would be classified as a sub-community include, but is not limited to:
- A new or growing community where the majority of the active members are members of our community.
- A community that is being privately advertised to members of our community.
- A community that is created with the purpose of being a "new" hangout spot.


#### 13. No Self-Harm Discussion

Any talk about wanting to harm oneself, or the encouragement of someone to hurt themselves is not allowed.

If you are suffering a crisis and need a professional to talk to please visit [Crisis Hotlines](https://en.wikipedia.org/wiki/Crisis_hotline) and [Suicide Crisis Hotlines](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines) to get in touch with professionals well equippped to handle these matters.


#### 14. No Spoilers

This includes spoilers relating to media content such as books, TV shows, movies ... etc.

This also includes the use of spoiler tags on platforms which support them or any other means to attempt to hide text that requires explicit interaction to view it.


#### 15. No False Reports

When reporting issues to staff, do not fabricate, exaggerate, or otherwise be dishonest or misleading with your statements. This includes any form of dishonesty by way of omission.

When questioned by a staff member we expect you to tell us the truth so we may properly understand and evaluate the issue or topic at hand.


#### 16. No Filter Bypasses or Exploiting

Attempting to bypass filters including but not limited to replacing characters, using fillers or obfuscation is not permitted.

Exploiting our community bots is not permitted, should you find a vulnerability or exploit please report it responsibly to staff.


#### 17. No Fake Links

Do not post fake links as these could potentially be malicious. This includes fake or misspelled image site links.


#### 18. No Begging

Begging will not be tolerated, this includes begging for staff privileges, community benefits or any game currency or item.


#### 19. No Feeding Trolls

Do not feed, instigate, provoke or entertain trolling. Doing so disrupts the flow of communication and will result in the same action being taken by staff against those involved.


#### 20. Not A Dating Community

The Linux.Chat community is not a dating community.


#### 21. No Politics Or Religion

Do not discuss politics or religion. These are sensitive topics which have a high likelyhood of causing strong emotions and conflict.

This also includes but is not limited to content supporting political views, religious movements or nations engaged in acts of human rights abuses.


#### 22. No Alts

Alts are not permitted. These are additional user accounts created on the same platform under the control of the same person.


#### 23. No Botting

The use of bots is not permitted. This includes any automated tools, scripts or LLMs (large language models) that simulate human activity, respond to messages, or perform tasks on behalf of a user.


#### 24. No Elitism

Elitism is not allowed in our community. This includes any form of negative or condescending behavior towards another person's choices, including but not limited to their choice regarding operating system, distribution, software, hardware, or programming language. Everyone's preferences and decisions are to be respected, and no one should feel belittled or judged based on their choices.

Constructive discussions and healthy debates are encouraged, but it is important to maintain a respectful and supportive atmosphere. Criticizing or bashing others for their choices not only fosters a toxic environment but also discourages diversity and innovation within the community.


#### 25. Topic Adherence in Channels

All members must ensure that discussions and content within topic-related channels remain aligned with the designated purpose and topic of those channels. This helps maintain organization, relevance, and the quality of interactions.

Off-topic content should be directed to the appropriate channels to keep the community focused and productive.


#### 26. No Idling in Staff Channels

Some platforms such as IRC have a staff channel, notably #linux-ops.

This channel exercises a relaxed no-idle policy. This means that once your issue has been dealt with, you're kindly requested to leave the channel.


#### 27. No Conspiracy Theories or Misinformation

To uphold the integrity and trust within the Linux.Chat community, it is crucial to prevent the dissemination of conspiracy theories and misinformation.

All members are required to:
- Refrain from sharing or promoting conspiracy theories or unverified information.
- Ensure the accuracy of information before sharing it within the community.

Spreading false information, rumors, or fake news compromises our community's trust and respect.



### Voice Chat Rules

Some of our communities support voice chat. Voice chat rules are additional rules for voice chat over and above the general rules.

General guidelines:
- **Stay On Topic:** Keep discussions relevant to the designated topic of the voice chat channel. Off-topic conversations should be moved to appropriate channels.
- **Avoid Interrupting:** Allow others to speak without interruption. Wait for your turn to contribute to the conversation.
- **Mute When Not Speaking:** Use the mute function when you are not speaking to minimize background noise.
- **Avoid Spamming:** Avoid excessive talking, loud noises, or repetitive sounds that can disrupt the conversation.
- **Be Mindful of Time:** Be concise and mindful of others' time. Allow everyone an opportunity to speak and contribute.
- **No Recording Without Permission:** Do not record the voice chat without obtaining explicit permission from all participants.
- **Use Appropriate Language:** Avoid strong and excessive language use. Maintain a professional and friendly tone.
- **Avoid Sensitive Topics:** Steer clear of sensitive or controversial topics such as politics and religion.
- **Report Issues:** If you encounter any issues or witness rule violations, report them to a moderator or staff member immediately.


#### 1. No External Software Use

External software such as soundboards, voice changers, and other sound altering software is not allowed.

This however excludes software used for the sole purpose of improving voice quality, ie. compressor, noise gate, noise reduction.


#### 2. No Excessive Joining/Leaving

Do not excessively join and leave channels.


## Protection from Retaliation

We are committed to maintaining a safe and open environment where members feel comfortable providing feedback and reporting concerns. We recognize the importance of feedback in improving our community and addressing issues promptly and effectively.

To support this, we have a strict policy against retaliation. Any member who provides feedback, raises concerns, or reports violations of this Code of Conduct will be protected from retaliation. Retaliation includes any form of adverse action taken against an individual for their participation in providing feedback and reporting concerns. This includes, but is not limited to, harassment, discrimination, negative treatment, or any form of intimidation.

Members are encouraged to speak up and share their experiences without fear of negative consequences. Our community values transparency and accountability, and we are dedicated to addressing all concerns fairly and promptly.


## Catalysts

Catalysts are essential building block of effective communities. No one is required to be a catalyst, but members who contribute to this role play a crucial part in maintaining a positive environment aiming to resolve issues through consensus and gentle guidance.

The Linux.Chat community does not automatically produce a stable culture of cooperative effort. Even in cases where cooperation is intended, misunderstandings and personality or culture incompatibilities can result in a chaotic and hostile environment. Catalysts help prevent and resolve misunderstandings, calm the waters when members have difficulties dealing with each other and provide examples of constructive behavior in environments where such behavior might not otherwise be the norm.

Catalysts try to resolve problems by fostering consensus, gently nudging participants in the direction of more appropriate behavior and by generally reducing and de-escalating the level of confrontation rather than confronting or antagonizing members.


### An Effective Catalyst

An effective catalyst is...

* **Relaxed.** To keep things calm, you yourself must be calm. Learn the skills of staying genuinely relaxed. Know your limitations; when you can't handle a difficult situation calmly, contact a moderator or staff member to assist.
* **Open-minded.** It is easy to make assumptions about other members' motivations. When you decide someone is behaving maliciously, you have made an assumption about their motivation which may be difficult to disprove. Try to make your assumptions about other members' motivations as positive as possible and assume good intent.
* **Responsible.** A community is a group activity with a strong need for responsible individual behavior. Rumors, innuendo and gossip can derail a community and ruin reputations. If everybody knows something is true, who is "everybody?" Did the person you are talking to get their information from documented, factual sources, or is it hearsay? If you cannot be sure of the answer to those questions, should you be passing on what they have said?
* **Unobtrusive.** It is not necessary to invoke authority to help solve a problem, and far better if you do not. Look for an opportunity to nudge the situation into a more productive track. Do not criticize or threaten members if a quiet change of subject, or a conversation on a completely different topic, can help de-escalate the situation.
* **Realistic.** Accept the personalities of the members and concentrate on problem resolution. Do not expect members to suddenly change their personalities to make problem resolution easier.
* **Careful.** Everything you say will be interpreted by the members with whom you interact. Consider how your remarks will be interpreted before you make them. Make sure the message you convey is the one you intend.
* **Attentive.** Understand the situation you have walked into __before__ you act. Question your assumptions. Look for signs that you may have misinterpreted the situation in order to avoid causing difficulties for a user who did not create the problem.
* **Minimalist.** Do not do more than you need to in order to resolve a problem. Concentrate on the resolution, and on collecting information you can think about later.
* **Courteous.** Even under pressure, courtesy costs little and impresses people a lot. It is not about whether interacting with the member is easy or difficult; it is about setting the right tone.
* **Cooperative.** Look for opportunities to get members involved in the resolution of their own and others' problems.
* **Problem solver.** Catalysts concentrate on solving problems, not bestowing blame. Treat the situation as the problem, accept members for who they are and try to figure out how best to help resolve the difficulty.
* **A member**. Remember that you are not in charge. Help the community along as unobtrusively as possible. Other catalysts are members as well, and nobody is perfect.


## Our Responsibilities

Linux.Chat staff are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

Linux.Chat staff have the right and responsibility to remove, edit, or reject messages, comments, commits, code, or any other contributions that are not aligned to this Code of Conduct.

Linux.Chat staff have the right to take certain action in response to any Linux.Chat community member exhibiting behaviors that they deem inappropriate, threatening, offensive, harmful or causes damage to the reputation of the Linux.Chat community.

Examples of such actions include, but are not limited to:
- Removal or limiting viewability of content.
- Limiting the ability to post content.
- A temporary, permanent or global ban from all Linux.Chat communities.
- A temporary, permanent or global ban from applying for a Linux.Chat account in the future.
- A temporary, permanent or global ban from participating as a Linux.Chat staff member.

Linux.Chat staff adhere to the policy of using special privileges granted to them only when absolutely necessary.


## Scope

This Code of Conduct applies to all Linux.Chat community spaces, herein collectively referred to as Linux.Chat:
- The Linux.Chat and Linux.Chat-related websites.
- The Linux.Chat public and private mailing lists.
- Any official public or private Linux.Chat forums, chat rooms, voice chats or channels.
- Any Linux.Chat public and private git repositories.
- Any additional Linux.Chat project spaces or communities added in the future.

When Linux.Chat community members contribute in discussion in or from our platforms, they represent the Linux.Chat community and its reputation.

Therefore, this Code of Conduct applies to:

- Public communication between Linux.Chat community members in Linux®-related community spaces or free software projects.
- Public communication between Linux.Chat community members at gatherings related to Linux.Chat and at Linux®-related events generally.
- Public Linux.Chat social media posts and comments by Linux.Chat community members on those social media posts.
- Private communication between Linux.Chat community members related to Linux.Chat communities or its members.
- Private communication between Linux.Chat community members and Linux.Chat staff.

This Code of Conduct also applies to official Linux.Chat communication, such as:

- Using an official Linux.Chat e-mail address.
- Acting as an appointed representative at an online or offline event.
- Posting via an official Linux.Chat social media account.

Representation of our community may be further defined and clarified by Linux.Chat staff.

If a concern about a past incident in any space is made known to Linux.Chat staff about a current Linux.Chat community member, Linux.Chat staff may take this into account when deciding whether the incident should have any consequences to the community member's participation in the Linux.Chat community.


## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by:
- Using builtin functionality (if supported), ie. "Report" menu item on any post or profile
- Contacting Linux.Chat staff directly
- Contacting Linux.Chat staff privately through their official Linux.Chat community account
- Sending an email to [abuse@Linux.Chat](mailto:abuse@linux.chat)

All complaints will be reviewed and investigated as quickly as possible, and will result in a response that is deemed necessary and appropriate to the circumstances.

The Linux.Chat staff are obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.


### Submitting Appeals

If you believe a decision or action taken against you is unjust, you have the right to submit an appeal. The appeals process follows the below outlined escalation procedure to ensure fairness and clarity:

1. **Private Discussion**: First, privately message the staff member involved. Approach the discussion calmly and clearly, even if you are upset. Taking a break before addressing the issue can help you remain composed. Try to resolve the matter amicably between yourselves and keep a record of your conversation.

2. **Community Escalation Medium**: If you are not satisfied with the outcome of the private discussion, use the community-specific support medium. State your issue calmly and clearly. Staff will review your case and try provide a resolution. Aggressive or disruptive behavior will likely result in being ignored. Community-specific support mediums are detailed below. Should there be no community-specific escalation medium, you may proceed to the next step.

3. **Formal Appeal Channel**: If the resolution from the above escalation is still unsatisfactory, compile a detailed log of the previous steps taken, including all relevant conversations. Provide a clear and concise summary of your issue and why you believe it has not been adequately addressed. Send this using the the relevant formal appeal communication medium below. Ensure that you follow the previous steps properly and avoid personal attacks to receive a timely response.


#### In-Community Escalation Mediums

* **#linux on Libera.Chat** Use the channel #linux-ops.
* **Linux.Chat on Discord** Use the #contact-staff channel to open a ticket.


#### Formal Appeal Submissions

Formal appeals may be submitted using the link to the relevant ban appeal form below.
* [#linux on Libera.Chat](https://linux.chat/linux-on-libera/irc-ban-appeal/)
* [Linux.Chat on Discord](https://linux.chat/linux-chat-on-discord/discord-ban-appeal/)
* [Linux.Community](https://linux.chat/linux-community-lemmy/linux-community-ban-appeal/)
* [Linux.Forum](https://linux.chat/linux-chat-support-forums/linux-forum-ban-appeal/)
* [Linux.Socialm](https://linux.chat/linux-social-on-mastodon/linux-social-ban-appeal/)

By adhering to this structured process, we aim to resolve disputes fairly and maintain the integrity of our community.


## DMCA Takedown Notices

DMCA takedown notices may be submitted to [dmca@Linux.Chat](mailto:dmca@Linux.Chat).

Please ensure your DMCA takedown notice meets the following requirements:
- The signature of the copyright owner or owner’s agent, in electronic form;
- Identification of the: (i) copyrighted work(s) infringed; (ii) the infringing activity; and (iii) the location of the infringing activity (by providing the URL);
- Contact information of the notice sender, including an email address;
- A statement that the notifier has a good faith belief that the material is not authorized by the intellectual property or copyright owner, its agent, or the law; and
- A statement that the information provided is accurate and the notifier is authorized to make the complaint on behalf of the intellectual property or copyright owner.

Please also take note of the DMCA takedown procedure:
- A copyright holder files a takedown notice, under penalty of perjury, with Linux.Chat claiming that the site is hosting infringing content owned by the copyright holder;
- Linux.Chat will provide the posting party (24) hours to remove the content, should they fail to action this, we will remove the content within (24) hours;
- The posting party then has the right to file a counter-notification, informing Linux.Chat that the content is not infringing; and
- If a counter-notice is filed, the service provider must re-host the content unless the original copyright holder files a lawsuit.


# Linux.Chat Inactive Account Policy

For communities where a Linux.Chat account needs to be created, the following inactive account policy applies.

In an effort to free up a wide range of account names, we may delete inactive accounts and associated personal data.

An account is considered inactive when:
- No content has been contributed by the account and it has been at least twelve (12) months since the account was last accessed; or
- It has been at least twenty-four (24) months since the account was last accessed.

For purposes of this policy, "accessed" means you have interacted with your account through one or more signed-in sessions from any device.

In order to warn the users concerned, an email will be sent to the e-mail address on file for the account providing the user twenty-eight (28) days to reply and request an appeal for the account to be restored. Once an account has been deleted and the warning period has passed, it can no longer be recovered. In order to use the specific Linux.Chat platform again, you'll need to create a new account.


# Contributing

Our community policies and information can be contributed to via our [Git Repository](https://gitlab.linux.community/linux-dot-chat/linux.chat-guidelines).


# Attribution

This Code of Conduct was adapted from:
- [FLOSS.social Code of Conduct](https://floss.social/about) as of December 21, 2022
- [Outreachy Community Code of Conduct](https://github.com/outreachy/website/blob/478c85f084de058ad8e4249d3c8d15d4025ed66d/CODE-OF-CONDUCT.md) as of January 28, 2022
  - The Outreachy Community Code of Conduct was adapted from the [Contributor Covenant, version 1.4](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html)

This document is made available under the terms of the [GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
