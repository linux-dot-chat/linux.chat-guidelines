# Linux.Chat Staff Manual


# New Staff Onboarding

New staff members are first onboarded into an advisory capacity, where they observe and advise while learning the ropes of their role without immediate pressure. This phase helps them understand community norms, community management techniques and decision-making processes, ensuring they are well-prepared for their responsibilities.

After the advisory phase, staff transition into the active phase, participating in the staff rotation schedules where they actively take on their staffing role for a period of time, then take a break as an advisory staff member.

This on-off rotation helps prevent burnout, ensures continuous learning, and maintains a dynamic and effective staffing team.

The benefits of new staff being onboarded in an advisory capacity include:
- **Observation and Learning:** New staff will observe current staff practices.
- **Advisory Role:** They will suggest actions they believe should be taken, while experienced staff members execute these actions.
- This phase helps new staff members understand their responsibilities without immediate pressure.

The advisory phase for new staff will last 3 months.

### Importance of the Advisory Staff Phase

The advisory staff phase is a crucial component of the staff recruitment process, designed to ensure new staff members are well-prepared and confident before assuming full responsibilities. This phase provides several key benefits:

#### Gradual Integration

The advisory phase allows new staff members to gradually integrate into their roles without being overwhelmed by immediate responsibilities. By observing and advising, new staff can learn the ropes at a manageable pace, which helps build their confidence and understanding of community dynamics.

#### Hands-On Learning

During the advisory phase, new staff members engage in hands-on learning. They observe experienced staff members handling various situations and provide input on potential actions. This real-world training helps them understand the nuances of their role, decision-making processes, and effective communication strategies.

#### Building Confidence

Entering a staff role can be daunting. The advisory phase helps new staff members build confidence by allowing them to contribute in a supportive environment. They can test their ideas and approaches with the guidance and support of experienced staff, which helps them feel more secure when they transition to full responsibilities.

#### Understanding Community Norms

Every community has its unique culture and norms. The advisory phase allows new staff members to familiarize themselves with these specific practices and standards from staffs perspective. They learn what is acceptable behavior, how to handle various situations, and the best ways to maintain a positive and welcoming environment, while at the same time de-escalating situations.

#### Reducing Mistakes

By advising rather than taking direct action initially, new staff members can avoid making mistakes that might occur due to inexperience. Experienced staff can review and approve their suggestions, providing a safety net that ensures the community is managed effectively, consistently and fairly.

#### Enhancing Collaboration

The advisory phase promotes collaboration between new and experienced staff members. This collaboration fosters a more team-oriented approach, where knowledge and best practices are shared. It helps new staff members feel integrated into the team and builds a strong foundation for future cooperation.

#### Smooth Transition to Becoming Active Staff

The advisory phase ensures a smooth transition to active staff New staff members enter their on months with a solid understanding of their role and responsibilities. They are better prepared to handle the demands of their role, leading to more effective and confident decision-making.

#### Continuous Improvement

Feedback from the advisory phase can be used to continuously improve the training and onboarding process. By evaluating the experiences of new staff members during this phase, the core staff team can refine its methods to ensure even better preparation for future staff.

#### New Staff Responsibilities

New staff members have several important responsibilities to ensure their active and effective engagement and integration within the team:

- **Awareness of Recent Events**: They are expected to stay informed about key recent events and discussions within their community. It is crucial they can express their awareness in a manner that is comfortable for them. This continuous engagement is essential for maintaining their role as an active and informed staff member.

- **Staying Up-to-Date with Communications**: It is mandatory they keep up with all staff communication channels. This includes joining and actively participating in our Mattermost instance. Regularly checking updates and communications will ensure they are always informed about the latest developments and can contribute effectively to the team.

- **Attending Monthly Meetings**: Staff members are required to make time for a 1-hour monthly meeting. Participation in this meeting is crucial for sharing ideas, discussing team updates, ongoing projects, and any issues that need to be addressed. Staff members must ensure they have a functioning microphone to actively participate in the discussions.


# Active Staff

After the advisory phase, new staff will move to become active staff members:
- **On Months:** Staff actively assume their roles within the community.
- **Off Months:** Staff members rest and reflect on their experiences, reducing the risk of burnout.

During off-months staff may choose to act in an advisory role to other staff.

The minimum off-month period is 1 month.


## Staff Roles

Our community operates under a centralized leadership model, where the ultimate responsibility for decision-making rests with the community owner.

While the community owner holds veto rights over any decision made by staff, these rights are only exercised when necessary to ensure that our community vision is upheld.

Constructive input from all staff members is highly valued, fostering a collaborative approach to problem-solving. This ensures that diverse perspectives are considered, leading to well-informed decisions that benefit the entire community.

Our goal is to create a positive and inclusive environment where everyone's voice is heard and respected.


**Core Team**

General responsibilities:
- Guides and advises other staff roles.
- Provides strategic direction for the community
- Ensures that the community's goals and values are upheld.
- Fills in moderation roles as required.

The core team role is global and pertains to all our platforms.


**Community Manager**

Community managers are generally established for each of the Linux.Chat communities.

General responsibilities:
- Oversees the overall health and growth of the community.
- Coordinates with other staff members and ensures that community guidelines are followed.
- Acts as a liason between the community and the Linux.Chat core team.
- Responsible for month reports back to the Linux.Chat core team on on community growth and issues.

The rotation schedule for this role is generally 3-6 months.


**Moderation Team**

Moderators are generally platform specific and well equipped to handle the intricacies for the platform they're moderating on.

General responsibilities:
- The primary responsibility of the moderation team is to ensure that the community adheres to our code of conduct. This involves monitoring interactions, identifying violations, and taking appropriate action to maintain a respectful and welcoming environment.
- Handles conflicts and resolves disputes between community members.

For communities supporting silencing and removing users with differnt permission sets, moderators generally are granted the ability to silence users only.

The rotation schedule forthis role is generally 1 month.


**Senior Moderation Team**

This role is referred to as an OP on IRC.

Senior moderators are generally platform specific and well equipped to handle the intricacies for the platform they're moderating on.

General responsibilities:
- Ensure that the community and moderators adheres to our code of conduct. This involves monitoring interactions, identifying violations, and taking appropriate action to maintain a respectful and welcoming environment.
- Handles conflicts and resolves disputes between community members and community and moderators.
- Assesses more serious user transgressions and decides if those warrant removal from the community instead of silencing
- In the absence of a community manager senior moderators report problem users and community issues to the core team.

For Linux.Chat communities supporting silencing and removing of users with different permission sets, senior moderators are granted the ability to remove users.

The rotation schedule for this role is generally 2 months.


**Outreach Coordinator**

Outreach coordinators can either be global roles or platform specific roles.

General responsibilities:
- Promotes the community to external audiences.
- Establishes partnerships with other communities and organizations.
- Engages in outreach activities to attract new members.
- Reports monthly to the core team on outreach efforts.

The rotation schedule for this role is generally 3-6 months.


**Appeals Team**

The appeals team is a global role and reviews appeals from all platforms.

General responsibilities:
- Reviews appeals thorougly and liaises with the core team, staff team and community to ensure fairness in outcomes from our appeals process.
- Suggests improvements to the community policies and the appeals process on recurring issues and feedback from the community.
- Upholds community values by ensuring that all actions taken and decisions align with the community's values and vision, promoting a respectful and supportive environment for all members.

The rotation schedule for this role is generally 3-6 months.


**Secretary Team**

The Secretary Team plays a crucial role in maintaining the efficiency and organization of the community. Their responsibilities include:
- **Meeting Coordination**: Organize and schedule staff meetings, including sending out invitations and reminders. Ensure that agendas are prepared in advance and distributed to all participants.
- **Minute Taking**: Record minutes during staff meetings and distribute them to all relevant parties. Ensure that action items are clearly documented and followed up on.
- **Feedback Collection**: Gather feedback from staff members on various aspects of community management and operations. Compile this feedback and present it to the core team for review.
- **Attendance Tracking**: Monitor staff attendance at meetings and events, and follow up with staff members who miss meetings to ensure they are informed of any important updates.


**Advisory Team**

The Advisory role is designated for staff members who are not on active duty, generally due to opting out of a staff rotation.

These members provide valuable expertise and guidance, drawing on their experience and knowledge to support the community when needed.

General responsibilities **as time permits**:
- Offer advice and insights on community matters, leveraging their experience to help active staff make informed decisions.
- Serve as a resource for active staff, providing consultation on complex issues or disputes that may arise.
- Participate in the review and development of community policies, ensuring they align with the community’s vision and values.
- Mentor newer staff members, sharing best practices and fostering their growth within the community.
- Assist with special projects or initiatives as needed, providing their expertise to ensure successful outcomes.
- Be available to step in temporarily during critical situations or emergencies to provide support and stability.

**Note:** Advisory staff members who are not actively involved within the community for a period of 6 months will be removed from the staff team. This ensures that we maintain a current and engaged team focused on the community. Should any staff member removed for inactivity feel they have time in the future to contribute back to the community, they are welcome to re-apply.


## Staff Support

Staff will receive ongoing support:
- **Supervision:** Experienced staff from the core team will oversee and support new staff.
- **Monthly Meetings:** Monthly check-ins are held to ensure that staff members are supported in their roles and have a platform to discuss any feedback, ideas, issues or concerns they may have. To ensure productive meetings, staff members should submit topics at least 3 days beforehand to be added to the agenda. During these meetings, staff members are encouraged to:
  - **Discuss Challenges and Achieve Consensus:** Share any difficulties they have encountered in their duties and seek advice or assistance from other staff members. Additionally, work towards achieving consensus on solutions and approaches to overcome these challenges.
  - **Provide Feedback:** Offer feedback on current processes and suggest improvements. This helps in continuously refining and optimizing community management practices.
  - **Review Progress:** Reflect on their performance and progress, discussing any goals they may have set and evaluating their achievements.
  - **Receive Training:** Participate in informational sessions on new tools, policies, or techniques that can help them perform their duties more effectively.
  - **Build Team Cohesion:** Engage in team-building activities and discussions to strengthen relationships and foster a supportive and collaborative team environment.

**Note:** These meetings are a vital part of building a cohesive and strong team. We understand that sometimes life happens and real-world issues can get in the way. However, we do have a reasonable expectation of regular attendance as it helps everyone stay connected and informed. Your participation is valuable and makes a big difference in our collaborative success.


### Documentation

The documentation for staff members will be maintained in our Git repositories. The specific locations for various types of documentation are as follows:
- **Rotation Schedules:** Available in the [Staff Rotations](https://gitlab.linux.community/linux-dot-chat/linux.chat-staff/-/blob/main/Rotation%20Schedule.md) section of our Git repository (staff only).
- **Role Descriptions:** Found in the [Staff Manual](https://gitlab.linux.community/linux-dot-chat/linux.chat-guidelines/-/blob/main/Staff%20Manual.md).
- **Training Materials:** Included within the [Staff Manual](https://gitlab.linux.community/linux-dot-chat/linux.chat-guidelines/-/blob/main/Staff%20Manual.md).
- **Meetings and Feedback:** Documented in the [Staff Meetings](https://gitlab.linux.community/linux-dot-chat/linux.chat-staff/meetings/) section of our Git repository.



# Staff Rotations


## Importance of Staff Rotations


### Preventing Burnout

Burnout is a significant concern for any dedicated team. When staff members remain in the same role for extended periods, they can experience fatigue, stress, and a sense of being overwhelmed. Rotating staff members to different roles or giving them breaks allows them to recharge and recover, reducing the risk of burnout. This practice ensures that staff members remain motivated, energetic, and effective in their roles, contributing positively to the community's health and well-being.


### Ensuring Fresh Perspectives and Ideas

Staff rotations are essential as they bring fresh perspectives and new ideas. When staff members change roles periodically, they can approach tasks with renewed enthusiasm and creativity. This continuous infusion of different viewpoints helps prevent stagnation and encourages innovation, ensuring that the community remains dynamic and adaptable to new challenges.


### Enhancing Skill Development

Staff rotations provide an excellent opportunity for skill development. By experiencing different roles, staff members can learn new skills, gain a broader understanding of the community's operations, and develop greater versatility. This cross-training approach not only benefits individual staff members by enhancing their professional growth but also strengthens the overall capability of the team.


### Promoting Fairness and Inclusivity

Regular staff rotations promote fairness and inclusivity within the community. They ensure that no single person or group holds a position of power for too long, reducing the potential for favoritism or the formation of cliques. This practice fosters a more inclusive environment where all members feel they have equal opportunities to contribute and take on leadership roles.


### Improving Problem-Solving and Decision-Making

When staff members rotate, they bring their unique experiences and knowledge to different areas of the community. This diversity in experience enhances problem-solving and decision-making processes, as different perspectives can lead to more comprehensive and effective solutions. It encourages collaborative thinking and the sharing of best practices across different parts of the community.


### Strengthening Team Cohesion

Staff rotations help build stronger team cohesion. As staff members work in various roles, they develop a better understanding and appreciation of their colleagues' responsibilities and challenges. This increased empathy and collaboration foster a more supportive and unified team, where members are more willing to help each other and work together towards common goals.


### Ensuring Continuity and Stability

Finally, staff rotations contribute to the continuity and stability of the community. By having multiple staff members familiar with different roles, the community becomes less vulnerable to disruptions caused by absences or departures. This redundancy ensures that essential functions can continue smoothly, maintaining the stability and resilience of the community.



## Staff Rotation Process

To ensure the smooth and effective rotation of staff members, the following process will be implemented:


### Rotation Schedule

Staff rotations will occur on a regular, predetermined schedule. This schedule will be established to balance the need for fresh perspectives with the necessity of continuity in key roles. Typically, rotations will occur every 1 to 6 months, but this may be adjusted based on the specific needs of the community.


### Role Assignment

Prior to the rotation, a review of current roles and upcoming needs will be conducted. Staff members will be assigned to new roles based on their skills, interests, and the  community’s requirements. Whenever possible, staff preferences will be taken into account to ensure a good match between the individual and their new responsibilities.

Staff not currently assigned an active role, or who wish to opt-out of active role assignemnt may take on the Advisory Role.


### Training and Handover

A handover period will be provided to ensure a smooth transition for staff entering a new role. Outgoing staff members will train their successors, sharing essential knowledge and best practices. This period will typically last 1 to 2 weeks and include:
- Role responsibilities.
- Key contacts and resources.
- Important ongoing projects and tasks.
- Common challenges and solutions.


### Support and Monitoring

Newly rotated staff members will receive ongoing support from experienced colleagues. Regular monthly meetings will be scheduled to address any questions, concerns, discuss any ideas and to provide additional guidance as needed. This support will be crucial in helping new staff members acclimate to their roles and perform effectively.


### Feedback and Evaluation

After each rotation cycle, feedback is encouraged from staff members regarding the process and their new roles. This feedback will be used to evaluate the effectiveness of the rotation and to make any necessary adjustments. Regular evaluations will help ensure that the rotation process remains beneficial for both the staff and the community.


### Addressing Challenges

To address any challenges that may arise during the rotation, a clear escalation path will be established. Staff members experiencing difficulties can seek assistance from their designated support personnel. Issues will be addressed promptly to minimize disruption and ensure a positive experience for all involved.


### Voluntary Participation

While staff rotations are encouraged to promote growth and prevent burnout, participation will be voluntary. Staff members who feel they are not ready for a rotation can opt out and assume an Advisory Role, with the understanding that they may be considered for future rotations.


### Continuous Improvement

The staff rotation process will be continuously reviewed and improved based on feedback and outcomes. This commitment to continuous improvement will help ensure that the rotation process remains effective and beneficial for the community and its members.



# Moderation Guidelines

As a moderator, you hold a position of trust and responsibility within the community. The following guidelines are designed to help you use your privileges effectively and ethically in enforcing our Code of Conduct:


## Moderation Values

Our moderation team strives to uphold values of fairness, transparency, and consistency when enforcing community guidelines. These values are essential in maintaining a respectful and welcoming environment for all members.


### Use Privileges Responsibly

As a staff member, it is crucial to use your privileges with integrity and responsibility.

- **Act Fairly:** Apply the Code of Conduct consistently and fairly to all members, without favoritism or bias.
- **Be Transparent:** Clearly communicate the reasons for any actions taken. An easy way to do this is to decide which rules were transgressed apon and the users history before taking action.
- **Prioritize Community Well-Being:** Use your privileges to foster a positive, respectful, and inclusive environment.


### Avoid Misuse of Power

The privileges granted to staff are intended to help maintain order and support the community. Misusing these privileges can undermine trust and community spirit, and may lead to their revocation.

- **Do Not Assert Dominance:** Do not use the privileges granted to you to try to win arguments, assert dominance, or prove a point in discussions.
- **Respect Privacy:** Do not share private information without explicit permission, including discussions with users relating to moderation actions taken against them.
- **Maintain Professionalism:** Always act in a professional manner, even in difficult situations. If the situation has caused heightened emotions, take a break and allow another member of the staff team to support you and take over.


## Proportional and Appropriate Actions

Sometimes, it can be challenging to decide on proportional or appropriate actions. It is always encouraged to ask other staff members for their opinions to ensure fair and consistent decisions. In the absence of staff being available, it is always a good idea to err on the side of the least severe action required.

- **Assess the Situation**: Evaluate the severity of the transgression and the context in which it occured.
- **Redirect the Conversation**: If a topic becomes heated or controversial, try to steer the conversation in another direction. This can sometimes help alleviate tension and prevent escalation. Use neutral or positive topics to shift focus and encourage a more constructive dialogue.
- **Try Warn First**: For minor infractions, try issue a warning to the user before taking more severe actions. This can be done subtly in public to avoid drawing unnecessary attention. eg. "that topic isn't really suited here", "can we please change the subject" ... etc.
- **Escalate Gradually:** If the behavior continues, gradually escalate the response. More permanent actions should be reserved only for severe cases.
- **Match the Response:** Ensure that the action taken matches the severity of the transgression. Minor issues should not result in harsh penalties, while serious infractions may require stronger responses.
- **Document Actions:** Comments on actions should be as clear as possible so that other staff can easily understand the reason for the action. This is very helpful if the user chooses to appeal the action taken against them.
- **Review and Reflect:** Review moderation actions to ensure they align with community values. This includes reviewing other staff's moderation actions and respectfully initiating discussions if any actions are deemed inconsistent with our Code of Conduct or Staff Manual. This should done in a constructive and positive manner.


### Ensure Due Process

To maintain fairness and transparency in our community, it is essential to ensure due process in all moderation actions. Users have the right to appeal and ask for an escalation of their issue.

It is therefore important to:
- **Gather Evidence:** Before taking any action, ensure sufficient evidence exists to support your decision. Decide which rules were transgressed and the impact on the community.
- **Communicate Clearly:** Generally when a user initiates contact, it is a good idea to allow them a few minutes to communicate and vent their frustration. Once they're clearly done doing this, continue to inform the affected user of the reasons for the action and provide an opportunity for them to explain or defend their actions. This is not a race, think and consider the matter carefully before responding, feel free to ask the staff team for advice or input if needed.

Should a user request an escalation or medium to take the matter further, one can advise them to refer to the relevant Ban Appeal form on our website or our [Feedback Form](https://linux.chat/give-feedback/), whichever is relevant.


### Promote Constructive Engagement

As a staff member, it is important to actively foster a positive and collaborative community atmosphere.

This can be done by:
- **Encouraging Positive Behavior:** Use your privileges to promote positive interactions and subtly discourage negative behavior.
- **Fostering Dialogue:** Encourage open and respectful dialogue by helping to gently mediate conflicts and misunderstandings.
- **Educating Members:** Provide guidance and education on community guidelines and expected behaviors by referring users to our Code of Conduct.


### Collaborate with Other Staff Members

Working together as a team is crucial to maintaining a cohesive and effective moderation process.

- **Team Coordination:** Engage with other staff members to ensure consistency and well-coordinated efforts.
- **Share Knowledge:** Share insights and experiences with your peers to improve overall practices.
- **Seek Support:** When in doubt, seek advice and support from fellow staff members or the core team.


### Escalate Problem Users and Issues

In some cases, it may be necessary to escalate problem users and issues to the core team to ensure they are handled appropriately.

- **Identify Serious Issues:** Recognize situations that require higher-level intervention, such as repeated violations or severe misconduct.
- **Document Thoroughly:** Provide detailed documentation of the issue, including evidence and context, to the core team.
- **Follow Up:** Ensure the issue is being addressed and provide any additional information or support as needed.


### Handling User Queries About Moderation Actions

Handling queries about moderation actions can be stressful, daunting and mentally taxing, especially when dealing with hostile users. It is encouraged to have a fellow staff member handle queries regarding moderation actions taken. This practice can provide a fresh take on the matter, ensure an unbiased approach, and prevent mental stress. Additionally, it allows for a second opinion which can enhance the fairness and transparency of the decision-making process, foster teamwork and collaboration among staff members, and build trust within the community by demonstrating a commitment to impartiality.

1. **Listen First, Respond Later**: Allow the user a few minutes to express their thoughts and concerns before responding.
2. **Take a Break**: Use this time to take a break, grab a coffee, and relax.
3. **Review Carefully**: After the user has had time to articulate their perspective, carefully read their message.
4. **Assess Fit**: This approach helps in understanding the user's viewpoint and gives insights into their personality and whether they are a good fit for the community.


### Handle Conflicts of Interest

Managing conflicts of interest is crucial to maintaining the integrity and trust of the moderation process.

- **Declare Conflicts:** If you have a personal relationship or conflict of interest with a member involved in a moderation issue, declare it and recuse yourself from the decision-making process.
- **Remain Impartial:** Strive to remain impartial and objective in all moderation actions. Your colleagues are there to help; feel free at any time to collaborate with other moderators or ask them to handle situations where a conflict of of interest exists.


### Continuous Improvement

Striving for continuous improvement is essential for effective moderation and the overall success of our community.

- **Learn and Adapt:** Continuously seek to improve your moderation skills and knowledge.
- **Feedback Loop:** Be open to feedback from the community and other staff, and use it to refine your approach.
- **Stay Updated:** Keep abreast of any changes to community guidelines, policies, or best practices. It is important to monitor staff communication channels regularly to stay informed and aligned with the team.


### Accountability

It is important for our staff to be accountable and humble. By following these guidelines, you can ensure that your use of moderator privileges contributes to the health, growth, and positive atmosphere of the community.

- **Be Accountable:** Take responsibility for your actions and decisions as a moderator.
- **Transparency in Actions:** Document and report any significant actions taken to the community manager, core team, or relevant channels.
- **Acknowledge Mistakes:** If you make a mistake, acknowledge it, apologize, and take steps to rectify the situation. Being humble in accepting feedback and learning from errors is crucial.


## Enforcement Guidelines

To maintain a respectful and welcoming environment, our moderation team is encouraged to follow a structured approach to enforcement. Below are the types of infractions we enforce, along with suggested ban durations for first-time occurrences and escalation for repeat offenders.


### Classes of Infractions

Infractions can be categorized into three classes: Minor, Moderate, and Major. Each class represents the severity of the violation and helps guide the appropriate enforcement action. This classification ensures that our response is proportional to the behavior and maintains a fair and respectful community environment.


#### Minor Infractions

Minor infractions are typically small disruptions that do not cause significant harm to the community but can still affect the overall experience if left unchecked. Examples include spamming, posting off-topic content, or mild disruptive behavior such as excessive use of caps or inappropriate jokes. While these actions may seem trivial, they can accumulate over time and create an unpleasant environment for community members. Addressing minor infractions promptly helps maintain a positive and focused atmosphere.

Examples of minor infractions include:
- Spamming or flooding the chat with repetitive messages.
- Posting off-topic content in channels with a designated topic.
- Mild disruptive behavior, such as excessive use of caps or inappropriate jokes.
- Failure to follow community guidelines after initial warning.


#### Moderate Infractions

Moderate infractions involve more serious breaches of community guidelines that have a noticeable impact on the well-being of other members. Such behavior can make community members feel unsafe or unwelcome, undermining the sense of trust and respect. By addressing moderate infractions, we aim to uphold the community's standards and ensure that all members feel valued and protected.

Examples of moderate infractions include:
- Harassment or bullying of other community members.
- Repeated minor infractions.
- Posting violent, offensive or inappropriate content that is not severe enough to warrant an immediate ban.
- Sharing misleading or false information that causes disruption.
- Engaging in heated arguments or confrontations that disrupt the community's harmony.


#### Major Infractions

Major infractions are severe violations that pose a significant threat to the safety and integrity of the community. Major infractions can cause substantial harm and fear among community members, leading to a toxic environment. It is crucial to address these actions decisively to protect the community and uphold our core values.

Examples of major infractions include:
- Sharing inappropriate or explicit content, including the use of explicit nicknames, usernames, aliases or profile content.
- Hate speech or discriminatory remarks based on race, gender, religion or other protected characteristics.
- Use of racial or ableist slurs.
- Threats of violence or personal harm.
- Violating privacy by sharing personal information without consent.
- Any behavior that endangers the safety of community members.


### Approaches to Moderation Actions: Warnings, Restrictions, and Removal

When enforcing our Code of Conduct, it is important to consider the impact of warning a user, limiting a user's ability to post content and removing them from the community entirely.
- **Warning Users:** Before taking any restrictive actions, it is often beneficial to issue a warning to the user. This gives them a chance to understand the consequences of their behavior and make amends without facing immediate restrictions.
- **Limiting Posting Abilities:** Temporarily restricting a user's ability to post can serve as a warning and an opportunity for the user to reflect on their behavior. This approach allows the user to remain part of the community while still facing consequences for their actions. It encourages rehabilitation and offers a chance for the user to correct their behavior without being completely isolated.
- **Removal from the Community:** In cases of severe or repeated violations, it may be necessary to remove a user from the community. This action is taken to protect other members and maintain a safe and respectful environment. While removal is a more drastic measure, it is sometimes necessary to uphold community standards and prevent further disruption.

Choosing the appropriate action depends on the severity and frequency of the user's behavior. By considering the specific circumstances and the user's history, staff can make informed decisions that balance the well-being of the community with the potential for individual improvement.


### Moderation Action Durations

Staff must consider the duration of a user's adherence to the rules when determining enforcement actions. If a user has demonstrated compliance with our Code of Conduct over an extended period and has been co-operative, they should be given the benefit of the doubt. Enforcement actions should lean towards being lenient, aiming to guide the user in the right direction. The reason for taking action is to help mould users into becoming positive, contributing members of the community.

A lenient approach encourages positive behavior and provides users with a fair opportunity to improve. However, it should also be noted that repeated unacceptable behavior, as well as new users who immediately engage in unacceptable behavior, is a problem and should be addressed accordingly.

When deciding on the duration of moderation actions affecting a user's ability to participate in the community, it's important for staff to work together as a team. Collaboration helps ensure that responses are fair and consistent. If you're ever unsure about the appropriate action to take, don't hesitate to reach out to other staff members. Discussing and deliberating together often leads to the best outcomes for the community. Remember, we're all here to support each other and make our community a positive and welcoming place for everyone.

Suggested durations on moderation actions taken are oulined below.


#### First-Time Occurrences

Suggested first-time infraction occurrence durations are outlined below:
- **Minor Infractions:**
  - Minimum Duration: 1 hours
  - Maximum Duration: 1 day
- **Moderate Infractions:**
  - Minimum Duration: 1 day
  - Maximum Duration: 3 days
- **Major Infractions:**
  - Minimum Duration: 1 hour (in almost all cases this should be a permanent ban)
  - Maximum Duration: Permanent ban

When determining the appropriate duration for a first-time infraction, it is essential to consider the user's contribution and value to the community. A new user who begins their journey with trolling, personal attacks or using a racial slur may warrant a much stronger moderation action (e.g., ban) compared to a long-time community member who uses an ableist slur in a moment of extreme frustration without thinking.

Taking into account the above suggested durations, collaboration among staff is extremely valuable to ensure fair and consistent application of these guidelines. Always discuss and deliberate with other staff members when unsure about the appropriate action to take. This approach helps maintain a positive and welcoming environment for everyone in the community.


#### Escalation for Repeat Offenders

The duration of the action taken is situational. If staff see an improvement in the user's behavior, a shorter duration may be used. This consideration must be weighed against the integrity and safety of the community.

Suggested repeat infraction occurrence durations are outlined below:
- **Minor Infractions:**
  - Second Occurrence: 3 hours to 2 days
  - Third Occurrence: 6 hours to 1 week
  - Subsequent Occurrences: 1 day to permanent ban
- **Moderate Infractions:**
  - Second Occurrence: 2 days to 2 weeks
  - Third Occurrence: 1 week to 1 month
  - Subsequent Occurrences: 2 weeks to permanent ban
- **Major Infractions:**
  - Second Occurrence: 1 day to permanent ban
  - Third Occurrence: 1 week to permannent ban
  - Subsequent Occurrences: 1 month to permanent ban


### Off-Topic Chat in Topic-Based Channels

While it is important to maintain the focus of discussions in topic-based channels, we understand that socializing and occasional off-topic conversations can contribute to a more friendly and welcoming atmosphere.

Moderation should strike a balance between enforcing channel guidelines and allowing users some leeway to socialize. Here are some guidelines to manage off-topic chat:

- **Be Lenient:** Allow occasional off-topic chat to promote a friendly environment. Users should feel comfortable and welcome to engage with each other.
- **Gently Redirect:** If off-topic discussions become too frequent or disrupt the channel’s purpose, kindly remind users to stay on topic and suggest moving the conversation to a more appropriate channel.
- **Set Clear Expectations:** Ensure that users are aware of the channel's purpose and the importance of staying on topic, but also that there is room for occasional friendly chat.
- **Foster Community:** Encourage users to use designated channels for socializing and general chat, highlighting the importance of keeping topic-based channels focused while also valuing the community spirit.

By balancing the need for topic-focused discussions with the value of social interaction, we can create an engaging and welcoming environment for all community members.


### Referring Users to Other Communities

The purpose of Rule 12 (No Sub-Communities) is to prevent users from soliciting (poaching) members from our community for self-promotion or other inappropriate reasons.

Nonetheless, it is acknowledged that users may benefit from resources and assistance available in other communities. Thus, it is permitted for users to refer others to established Linux-related communities, projects, platforms, and relevant resources in response to their needs. Rule 12 is not intended to prohibit such beneficial exchanges.

- **Solicited Sharing:** Users are permitted to share links to official Linux-related communities, projects, platforms, and resources in response to specific requests for assistance or when such sharing can aid the user or be beneficial to the community.
- **Relevant Assistance:** All shared links and resources must be relevant to the user's query or of general interest to the community and should contribute positively to the user's experience.
- **Respect and Appropriateness:** Resources must be shared respectfully and must align with our Code of Conduct.

Our goal is to foster a supportive environment where users can access the best possible information and assistance while maintaining the integrity and focus of our community.



## Platform Guides


## Libera.Chat - #linux

The channel management bot on Libera.Chat is called `litharge`, it supports ban durations in the format of `1h`, `1d`, `1w`.

To set a 3 month action, one would use `90d`.

The below is taken in summary from the (litharge documentation)[https://libera.chat/guides/bots].


### How to Target Users

To determine if targeting an account or IP address is appropriate, use the command `/whois <nick>`.

- **Logged-in Users:** The first line of output will state `Logged in as`, followed by the username. Target these users using `$a:<username>`.
- **Non-logged-in Users:** These users may have an IPv4 address, IPv6 address, or hostname.
  - For IPv4 addresses, target them using `*!*@<IPv4 address>`.
  - For IPv6 addresses, use the first 4 components of the address (e.g., for A:B:C:D:E:F:G:H, use `*!*@A:B:C:D:*`). This prevents the user from easily changing their IP address within the commonly assigned /64 range.


### Query User History

To query a users previous history one can use:
```
query <target>
```


### Quieting

To quiet a user, send the following message to `litharge`:
```
q #linux <target> <duration> <reason>
```

Removing a quiet on a user can be done using:
```
uq <id>
```


### Banning

To ban a user, send the following message to `litharge`:
```
b #linux <target> <duration> <reason>
```

Unbanning a user can be done using:
```
ub <id>
```


### Editing Durations

To edit the duration of an action, use:
```
edit <id> <duration>
```


### Editing Reasons

To edit the reason for an action, use:
```
mark <id> <reason>
```


### Unbanning Ozone Klines

False positive klines can be removed by opping yourself in #linux and using `/msg ozone unkline <nick>`.



## Discord - Linux.Chat


### Targetting Users

The easiest way to interface with our bot `Slinky` is to use the following command for any action needed to be taken on a user:
```
sb!userinfo <userid>
```

The `userid` is obtained by enabling Developer Mode in your Discord account settings and right-clicking on a user and selecting `Copy User ID`.

Slinky will display a menu of various actions one can perform and display additional information about the user, including notes and join information.


### Warning

A user can be warned by issuing the following command:
```
sb!warn <userid>
```

`Slinky` will then prompt for the required information, duration, and reason.

Once the warning has been issued, `Slinky` will message the user with the warning. **Note:** The user will not get the message if they have disabled messages via DMs.

It is encouraged to use the `sb!userinfo` command as this also displays notes about the user and previous history.


### Muting

A user can be muted using the following command:
```
sb!mute <userid>
```

`Slinky` will then prompt for the required information, duration, and reason.

Once the mute has been issued, `Slinky` will message the user with the reason. **Note:** The user will not get the message if they have disabled messages via DMs.

The mute functionality in `Slinky` will drop the user into a special view of the Discord server where they will only see the Rules channel and a channel with information "Why am I here". This is called the Shadow Realm.

It is encouraged to use the `sb!userinfo` command as this also displays notes about the user and previous history.

A user can be unmuted using the following command:
```
sb!unmute <userid>
```


### Banning

A user can be banned using the following command:
```
sb!ban <userid>
```

`Slinky` will then prompt for the reason.

Once the ban has been issued, `Slinky` will message the user with the reason along with the appeals link. **Note:** The user will not get the message if they have disabled messages via DMs.


### Getting Help

`Slinky` has a wide variety of commands that can be used, depending on the privileges granted to a user.

One can display help using the following command:
```
sb!help
```

And the following for a specific command or command group:
```
sb!help moderation
```



## Linux.Chat - Linux.Social

To moderate on Linux.Social, select a user and click "Open Moderation Interface." Both users and federated instances can be moderated.

### Warning

Send a message to warn a user.

### Freeze Account

This action prevents the user from accessing their account, while preserving all content. This restriction can be reversed, and it is only applicable to local users on your server.

### Mark as Sensitive

All media from the account will be flagged as sensitive.

### Restrict Account

Previously called "silencing," this action makes the user's content hidden from everyone except their followers. Their posts and information are still searchable and visible through mentions and following, but not publicly displayed.

Currently, restricting an account does not affect federated servers. A locally restricted account remains unrestricted on other servers. Restrictions can be undone.

### Suspension

Suspending a Mastodon account effectively removes it. The account will not appear in searches, and its profile, posts, uploads, and followers will be inaccessible. However, the data is retained in the admin back-end for 30 days, allowing the user to resolve issues with instance admins and potentially have the account reinstated.

If reinstated within 30 days, all data becomes publicly accessible again without loss. After 30 days, or upon immediate deletion by admins, all user data is permanently removed. The account can be unsuspended but will lack any data (statuses, profile info, images).

For remote accounts, suspension will remove all local follow relationships, which will not be restored if the remote account is unsuspended.

### Blocking a Federated Domain

To efficiently manage a large number of users from a problematic server, you can apply a domain block with varying levels of severity:

- **Reject Media:** Prevents processing of any files from the server, including avatars, headers, emojis, and media attachments.
- **Restrict:** Similar to restricting all past and future accounts from the server, previously known as "silencing."
- **Suspend:** Similar to suspending all past and future accounts from the server. No content from the server is stored locally, except usernames. This action will remove all follow relationships between local accounts and accounts on the suspended server, and they will not be restored if the server is unsuspended.




## Linux.Chat - Linux.Community

TODO



## Linux.Chat - Linux.Forum

TODO

### Warning Users

TODO

### Banning Users

TODO

### Deleting Threads

TODO

### Locking Threads

TODO



# Staff Systems


## Git Repositories

Our Code of Conduct, Staf Manual and staff meetings are available in our Git repositories and is available [here](https://gitlab.linux.community/linux-dot-chat/linux.chat-guidelines).

The staff meeting minutes are not publically available and can be found [here](https://gitlab.linux.community/linux-dot-chat/linux.chat-meetings).


## Ticket System

Our ticket system handles feedback from users and ban appeals and is available [here](https://support.linux.chat).


## Staff Chat

We use Mattermost for collaboration between all staff teams and is available [here](https://staff.linux.chat).



# Attribution

This "Linux.Chat Staff Manual" is published under the [GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
